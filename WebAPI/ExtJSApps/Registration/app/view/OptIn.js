/*
 * File: app/view/OptIn.js
 *
 * This file was generated by Sencha Architect version 2.2.0.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.2.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('RegistrationApp.view.OptIn', {
    extend: 'Ext.form.Panel',
    alias: 'widget.OptInAlias',

    layout: {
        align: 'stretch',
        type: 'vbox'
    },
    bodyPadding: 20,
    title: 'Opt In',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'top',
                    itemId: 'ToolBarForgotUsername',
                    layout: {
                        pack: 'end',
                        type: 'hbox'
                    },
                    items: [
                        {
                            xtype: 'button',
                            itemId: 'backButtonId',
                            iconAlign: 'right',
                            text: 'Back'
                        },
                        {
                            xtype: 'tbseparator'
                        },
                        {
                            xtype: 'button',
                            itemId: 'continueButtonId',
                            iconAlign: 'right',
                            text: 'Finish'
                        }
                    ]
                }
            ],
            items: [
                {
                    xtype: 'panel',
                    border: false,
                    height: 44,
                    items: [
                        {
                            xtype: 'label',
                            style: 'font-weight:bold;font-size:120%',
                            text: 'Opt In Choices.  Hear from our sponsors (this helps keep code camp free) and SVCC Kids Info'
                        }
                    ]
                },
                {
                    xtype: 'panel',
                    border: false,
                    padding: 5,
                    bodyPadding: 5,
                    items: [
                        {
                            xtype: 'checkboxfield',
                            margin: '0 0 20 0',
                            fieldLabel: 'Interested in bring your kid to Code Camp\'s special classes for them?  Check here and you will be contacted about details and special confirmation requirements',
                            labelWidth: 300,
                            name: 'optInSvccKids',
                            boxLabel: '',
                            checked: true,
                            inputValue: '1',
                            uncheckedValue: '0'
                        },
                        {
                            xtype: 'checkboxfield',
                            margin: '0 0 20 0',
                            fieldLabel: 'Accept Emails From Sponsors Offering Specials Only Available To Code Camp Attendees.  <br/>All email to you include in the subjects <i>[Sponsored Specials]</i>',
                            labelWidth: 300,
                            name: 'optInSponsoredMailingsLevel',
                            boxLabel: '',
                            checked: true,
                            inputValue: '1',
                            uncheckedValue: '0'
                        },
                        {
                            xtype: 'checkboxfield',
                            margin: '0 0 20 0',
                            fieldLabel: 'Accept general emails From Sponsors<br/>All email to you include in the subject <i>[Sponsored Email]</i>',
                            labelWidth: 300,
                            name: 'optInSponsorSpecialsLevel',
                            boxLabel: '',
                            checked: true,
                            inputValue: '1',
                            uncheckedValue: '0'
                        },
                        {
                            xtype: 'fieldset',
                            padding: 5,
                            title: 'Job Opportunities From Our Sponsor Companies',
                            items: [
                                {
                                    xtype: 'label',
                                    text: 'Interested in hearing about tech jobs from our sponsors?  If so enter a couple keywords that they might contact you about'
                                },
                                {
                                    xtype: 'textfield',
                                    margin: '5 0 5 0',
                                    width: 300,
                                    fieldLabel: '',
                                    name: 'optInTechJobKeyWords',
                                    maxLength: 60
                                }
                            ]
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});