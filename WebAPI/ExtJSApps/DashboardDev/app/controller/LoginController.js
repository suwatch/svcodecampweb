/*
 * File: app/controller/LoginController.js
 *
 * This file was generated by Sencha Architect version 2.1.0.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.1.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.1.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('App.controller.LoginController', {
    extend: 'Ext.app.Controller',

    onLoginButtonIdClick: function(button, e, options) {

        // verify login and if success close

        var values = Ext.getCmp('loginFormId').getForm().getValues();
        var myMask = new Ext.LoadMask(Ext.getBody(), 
        {msg:"Logging In..."});
        var task = new Ext.util.DelayedTask(function(){
            myMask.show();
        });

        task.delay(500);
        Ext.Ajax.request({ 
            url:'/rpc/Account/Login', 
            params: values,
            method: 'POST',
            success: function(r, o) { 
                task.cancel();
                if (myMask.isVisible) {
                    myMask.hide();
                }
                var win = Ext.getCmp("loginWindowId");
                win.close();

            },
            failure: function(r,o) {
                task.cancel();
                if (myMask.isVisible) {
                    myMask.hide();
                }  

                Ext.Msg.alert("Login Failed. Please Try Again");
            }
        });


    },

    onButtonClick: function(button, e, options) {
        // logout
        var myMask = new Ext.LoadMask(Ext.getBody(), 
        {msg:"Logging out..."});
        var task = new Ext.util.DelayedTask(function(){
            myMask.show();
        });

        task.delay(500);
        Ext.Ajax.request({ 
            url:'/rpc/Account/LogOut', 
            params: {},
            method: 'POST',
            success: function(r, o) { 
                task.cancel();
                if (myMask.isVisible) {
                    myMask.hide();
                }
                Ext.Msg.alert("Logged Out.  Refresh Page To Login again");


            },
            failure: function(r,o) {
                task.cancel();
                if (myMask.isVisible) {
                    myMask.hide();
                }  

                Ext.Msg.alert("Logout Failed");
            }
        });

    },

    init: function(application) {
        this.control({
            "#loginButtonId": {
                click: this.onLoginButtonIdClick
            },
            "#logoutButtonId": {
                click: this.onButtonClick
            }
        });
    }

});
