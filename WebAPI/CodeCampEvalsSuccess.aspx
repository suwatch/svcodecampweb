<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="CodeCampEvalsSuccess" Title="Untitled Page" Codebehind="CodeCampEvalsSuccess.aspx.cs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="parentContent" Runat="Server">
<br />
<br />
<h1>Survey Information Updated</h1>
<p>You can always come back and log into your account and update your survey if you think
of other things you want to add or change.  Simply log into your account, then choose the option
on the menu on the left entitled Code Camp Survey.  Thanks for taking the time.</p>
<br />
Your Code Camp Team!
</asp:Content>

