/*
 * File: app/view/MyForm.js
 *
 * This file was generated by Sencha Architect version 2.1.0.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.1.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.1.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('MyApp.view.MyForm', {
    extend: 'Ext.form.Panel',

    height: 250,
    width: 400,
    bodyPadding: 10,
    title: 'My Form',
    url: 'save.aspx',

    initComponent: function() {
        var me = this;

        me.initialConfig = Ext.apply({
            url: 'save.aspx'
        }, me.initialConfig);

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'radiogroup',
                    fieldLabel: '',
                    allowBlank: false,
                    items: [
                        {
                            xtype: 'radiofield',
                            name: 'MyCB',
                            boxLabel: 'CB1',
                            inputValue: 'chb1'
                        },
                        {
                            xtype: 'radiofield',
                            name: 'MyCB',
                            boxLabel: 'CB2',
                            inputValue: 'chb2'
                        }
                    ]
                },
                {
                    xtype: 'button',
                    formBind: true,
                    text: 'Press Me When Form Validated'
                },
                {
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'Label',
                    allowBlank: false
                },
                {
                    xtype: 'button',
                    text: 'save',
                    listeners: {
                        click: {
                            fn: me.onButtonClick,
                            scope: me
                        }
                    }
                }
            ]
        });

        me.callParent(arguments);
    },

    onButtonClick: function(button, e, options) {

        debugger;
        this.getForm().submit();
    }

});