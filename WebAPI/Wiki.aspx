﻿<%@ Page Language="C#" MasterPageFile="~/RightRegister.master" AutoEventWireup="true"
    Inherits="Wiki" Title="Silicon Valley Code Camp Wiki" Codebehind="Wiki.aspx.cs" %>
    
    
    
<asp:Content ID="SessionsContent" ContentPlaceHolderID="MainContent" runat="server">



<div class="mainHeading">Wiki</div>



<div class="pad">
    Associated with Silicon Valley CodeCamp is a Wiki generously donated by
    <a href="http://pbwiki.com/">PBWiki</a>.&nbsp; This Wiki is really a companion 
    to this web site.&nbsp; It includes more detailed information about sessions as 
    well as things like code samples.&nbsp; Though normally you don&#39;t access the 
    wiki directly, there is a <a href="http://codecamp.pbwiki.com/">wiki home page</a> here.</div>



</asp:Content>


