﻿
namespace App_Code
{
    public enum EmailSubscriptionEnum
    {
        AllEmails = 0,
        NoEmail = 1,
        SystemDisabled = 2
    }
}