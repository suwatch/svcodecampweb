using System.Drawing;

/// <summary>
/// Summary description for ProgramConstants
/// </summary>
/// 
namespace CodeCampSV
{
    public static class ProgramConstants
    {
        public static string crlf = "\r\n";
        public static Color FailureBackground = Color.Red;
        public static Color FailureForeGround = Color.White;
        public static Color SuccessBackground = Color.Green;
        public static Color SuccessForeGround = Color.White;

        //public static string RoleDeveloper = "Developers";
        //public static string RoleAdmin = "Administrators";
        //public static string RoleSuperAdmin = "Super-Administrators";
    }
}