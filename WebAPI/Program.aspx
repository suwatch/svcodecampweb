﻿<%@ Page Language="C#" MasterPageFile="~/RightRegister.master" AutoEventWireup="true"
    Inherits="Program" Title="Program" Codebehind="Program.aspx.cs" %>
    
    
    
<asp:Content ID="SublinksProgram" ContentPlaceHolderID="Sublinks" runat="server">
    <asp:Menu ID="subMenu" runat="server" DataSourceID="SiteMapProgram" SkinID="subMenu">
    </asp:Menu>
</asp:Content>



<asp:Content ID="ProgramContent" ContentPlaceHolderID="MainContent" runat="server">



<div class="mainHeading">What&#39;s Gonna Happen</div>



<div class="pad">
    Expect lots of education, networking and good food.&nbsp; The basic format is 
    that we start with registration Saturday morning outside the main area where 
    classes happen (check the News Page for details).&nbsp; After that, we will have a 
    short briefing in the quad followed by lots of sessions.&nbsp; Lunch follows 
    after that, more sessions, then usually a BBQ for volunteers and speakers provided by one of our 
    generous sponsors.&nbsp; Sunday, there is no opening meeting.&nbsp; If you 
    registered Saturday, you can go right to your session.&nbsp; If not, register 
    first, then go to your session.  Make sure to bring your badge from Saturday registration.<br />
    <br />
    Computer labs are setup and there should be free wireless available (check your 
    sign in documents you received from registration).<br />
    <br />
    That&#39;s about it!&nbsp; See you at camp!<br /><br />
</div>



</asp:Content>


