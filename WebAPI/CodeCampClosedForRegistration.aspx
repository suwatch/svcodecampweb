﻿<%@ Page Title="" Language="C#" MasterPageFile="~/RightRegister.master" AutoEventWireup="true" Inherits="CodeCampClosedForRegistration" Codebehind="CodeCampClosedForRegistration.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Sublinks" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

<div class="mainHeading">
    Registration is Closed for This Year</div>
</div>

<div class="pad">

<h2>&nbsp;</h2>
    <h2>We are sorry to have to let you know that we have reached our capacity and will not be able 
to offer any more registrations this year.</h2>
    <br/>
<h2 style="background-color: #FF6600">No on-site registration</h2>
    <p>&nbsp;</p>
    
<p>To Attend Code Camp you must be registered and pick up a badge.  In the future, we will
attempt to get more space than we have now, however, for this year, we can not accept any more registrations</p>
</div>
   
</asp:Content>

