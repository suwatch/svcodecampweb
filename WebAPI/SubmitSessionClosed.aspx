﻿<%@ Page Title="" Language="C#" MasterPageFile="~/RightNote.master" AutoEventWireup="true" Inherits="SubmitSessionClosed" Codebehind="SubmitSessionClosed.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Sublinks" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">



    <div runat="server" id="DIVMain">
    <h2>
       Session Submissions Are Currently Closed</h2>

    <hr />


    <p>We accept submissions for sessions starting about 4 months before Silicon Valley Code Camp and close them about 4 to 6 weeks before the event.&nbsp; 
        Our event is typically early fall every year.&nbsp; We try and set the date as 
        early as possible not to overlap with other large industry events.&nbsp; Thanks 
        for stopping by.</p>
  </div>


</asp:Content>

