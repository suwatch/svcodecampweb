//  This file is meant as a starting point only.  it should be copied into working source tree only if there is not
//  an existing file with this name in it already.
using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;

namespace CodeCampSV
{
    public partial class SessionTagsQuery
    {
        public int? CodeCampYearId;

        public bool? WithTagDescription;

        public bool? WithAllTagsAllYears;
    }
}
